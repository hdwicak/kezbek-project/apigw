FROM node:18.13.0-alpine3.16 As development

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm cache clean --force

RUN npm install rimraf

RUN npm i -g @nestjs/cli

RUN npm ci

COPY . .

RUN npm run build

CMD [ "node", "dist/main.js" ]