import { Test, TestingModule } from '@nestjs/testing';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ClientKafka } from '@nestjs/microservices';

describe('AppController', () => {
  let appController: AppController;
  let clientKafka: ClientKafka;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [AppController],
      providers: [
        AppService,
        {
          provide: 'SERVICE_APIGW',
          useValue: {
            emit: jest.fn(),
          },
        },
      ],
    }).compile();

    appController = app.get<AppController>(AppController);
  });

  describe('App Controller', () => {
    it('should return processing transaction"', () => {
      const input = {
        trx_date: new Date(),
        partner_code: '1XCX1',
        partner_key: 'aabbccddeeff',
        promo_code: 'DEC001',
        customer_email: 'jdoe@gmail.com',
        payment_wallet: 'BKL',
        order_id: 'ORXB777N',
        purchase_quantity: 2,
        purchase_amount: 1000000,
        description: 'lorem ipsum',
      };
      const result = {
        status: 'SUCCESS',
        message: 'Processing Transactions',
        data: [],
      };
      expect(appController.submitTransaction(input)).toEqual(result);
    });
  });
});
