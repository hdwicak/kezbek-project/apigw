import { Inject, Injectable } from '@nestjs/common';
import { ClientKafka } from '@nestjs/microservices';
import { KezbekMessageDto } from './dto/kezbek-message.dto';
import { TransactionDataDto } from './dto/transaction-data.dto';
import { HttpService } from '@nestjs/axios';

@Injectable()
export class AppService {
  httpService: HttpService;
  constructor(
    @Inject('SERVICE_APIGW')
    private readonly transactionClient: ClientKafka,
  ) {
    this.httpService = new HttpService();
  }

  getHello(): string {
    return 'Hello World!';
  }

  emitTransaction(kezbekMessageDto: KezbekMessageDto) {
    this.transactionClient.emit(
      'add-transactions',
      JSON.stringify(kezbekMessageDto),
    );
  }

  async handleHttpTransaction(url, data: TransactionDataDto){
    return this.httpService.post(url, data);
  }
}
