import { ApiProperty } from '@nestjs/swagger';
import {
  IsDateString,
  IsEmail,
  IsNotEmpty,
  IsNumber,
  IsString,
  Min,
} from 'class-validator';

export class KezbekMessageDto {
  @ApiProperty()
  @IsDateString()
  @IsNotEmpty()
  trx_date: Date;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  partner_code: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  partner_key: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  promo_code: string;

  @ApiProperty()
  @IsEmail()
  @IsNotEmpty()
  customer_email: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  payment_wallet: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  order_id: string;

  @ApiProperty()
  @IsNumber()
  @IsNotEmpty()
  @Min(1)
  purchase_quantity: number;

  @ApiProperty()
  @IsNumber()
  @IsNotEmpty()
  @Min(1)
  purchase_amount: number;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  description: string;
}
