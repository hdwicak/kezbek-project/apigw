import { Body, Controller, Get, Inject, Logger, Post, UseGuards } from '@nestjs/common';
import { AppService } from './app.service';
import { KezbekMessageDto } from './dto/kezbek-message.dto';
import { ClientKafka, EventPattern, Payload } from '@nestjs/microservices';
import * as dotenv from 'dotenv';
import { TransactionDataDto } from './dto/transaction-data.dto';
import { ApiTags } from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';

dotenv.config();

@ApiTags('apigw')
@Controller('transaction')
export class AppController {
  logger: Logger;
  constructor(
    private readonly appService: AppService,

    @Inject(process.env.KAFKA_TOKEN || 'SERVICE_APIGW')
    private readonly client: ClientKafka,
  ) {
    this.logger = new Logger(AppController.name);
  }

  @Post()
  @UseGuards(AuthGuard('api-key'))
  submitTransaction(@Body() kezbekMessageDto: KezbekMessageDto) {
    this.appService.emitTransaction(kezbekMessageDto);
    this.logger.log('[KEZBEK-APIGW] processing new transactions from partner');
    return {
      status: 'SUCCESS',
      message: 'Processing Transactions',
      data: [],
    };
  }

  @EventPattern('success-transaction')
  async handleSuccessTransaction(@Payload() data: TransactionDataDto) {
    try {
      this.logger.log(
        '[KEZBEK-APIGW] processing message from topic "success transaction" and make api call to partner',
      );
      const url =
        process.env.URL_KEZBEK_PARTNER ||
        'https://transaction.free.beeceptor.com/transaction-status';
      await this.appService.handleHttpTransaction(url, data);
      this.logger.log(
        '[KEZBEK-APIGW] finish processing transactions callback to partner',
      );
    } catch (err) {
      this.logger.error(err);
    }
  }

  @EventPattern('error-transaction')
  async handleErrorTransaction(@Payload() data: TransactionDataDto) {
    try {
      this.logger.log(
        '[KEZBEK-APIGW] processing message from topic "error-transaction" and make api call to partner',
      );
      const url =
        process.env.URL_KEZBEK_PARTNER ||
        'https://transaction.free.beeceptor.com/transaction-status';
      await this.appService.handleHttpTransaction(url, data);
      this.logger.log(
        '[KEZBEK-APIGW] finish fallback transactions callback to partner',
      );
    } catch (err) {
      this.logger.error(err);
    }
  }
}
